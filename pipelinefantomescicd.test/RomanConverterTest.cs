namespace pipelinefantomescicd.test
{
    public class RomanConverterTest
    {
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public void Test1_3(int number)
        {
            var romanConverter = new RomanConverter();
            var romanNumber = romanConverter.ConvertToRoman(number);
            string expectedNumber = new String('I', number);
            Assert.Equal(expectedNumber, romanNumber);
        }

        [Fact]
        public void Test4()
        {
            //Arrange
            var romanConverter = new RomanConverter();
            //Act
            var romanNumber = romanConverter.ConvertToRoman(4);
            //Assert
            Assert.Equal("IV", romanNumber);
        }

        [Theory]
        [InlineData(5)]
        [InlineData(6)]
        [InlineData(7)]
        [InlineData(8)]
        public void Test5_8(int number)
        {
            var romanConverter = new RomanConverter();
            var romanNumber = romanConverter.ConvertToRoman(number);
            string expectedNumber = "V" + new String('I', number - 5);
            Assert.Equal(expectedNumber, romanNumber);
        }


        [Fact]
        public void Test9()
        {
            //Arrange
            var romanConverter = new RomanConverter();
            //Act
            var romanNumber = romanConverter.ConvertToRoman(9);
            //Assert
            Assert.Equal("IX", romanNumber);
        }

        [Theory]
        [InlineData(10)]
        [InlineData(11)]
        [InlineData(12)]
        [InlineData(13)]
        public void Test10_13(int number)
        {
            //Arrange
            var romanConverter = new RomanConverter();
            string expectedNumber = "X" + new String('I', number - 10);
            //Act
            var romanNumber = romanConverter.ConvertToRoman(number);
            //Assert
            Assert.Equal(expectedNumber, romanNumber);
        }
        [Fact]
        public void Test14()
        {
            //Arrange
            var romanConverter = new RomanConverter();
            //Act
            var romanNumber = romanConverter.ConvertToRoman(14);
            //Assert
            Assert.Equal("XIV", romanNumber);
        }

        [Theory]
        [InlineData(15)]
        [InlineData(16)]
        [InlineData(17)]
        [InlineData(18)]
        public void Test15_18(int number)
        {
            //Arrange
            var romanConverter = new RomanConverter();
            string expectedNumber = "XV" + new String('I', number - 15);
            //Act
            var romanNumber = romanConverter.ConvertToRoman(number);
            //Assert
            Assert.Equal(expectedNumber, romanNumber);
        }
    }
}