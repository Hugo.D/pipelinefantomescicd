﻿namespace pipelinefantomescicd
{
    public class RomanConverter
    {
        public string ConvertToRoman(int nombreArabe)
        {
            var nombreRomain = nombreArabe switch
            {
                1 or 2 or 3 => new String('I', nombreArabe),
                4 => "IV",
                5 or 6 or 7 or 8 => "V" + new String('I', nombreArabe - 5),
                9 => "IX",
                10 or 11 or 12 or 13 => "X" + new String('I', nombreArabe - 10),
                14 => "XIV",
                15 or 16 or 17 or 18 => "XV" + new String('I', nombreArabe - 15),
                _ => "Pas de nombre",
            };
            return nombreRomain;
        }
    }
}